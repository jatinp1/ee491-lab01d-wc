# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall
SRC = wc.c file_processor.c close_file.c

TARGET = wc

all: $(TARGET)

wc: $(SRC)
	$(CC) $(CFLAGS) -o $(TARGET) $(SRC)

clean:
	rm $(TARGET)

test: $(TARGET) $@
	@echo ""
	wc /tmp/ee491f-wc-test-files/test1
	./$< /tmp/ee491f-wc-test-files/test1
	@echo ""
	wc /tmp/ee491f-wc-test-files/test2
	./$< /tmp/ee491f-wc-test-files/test2
	@echo ""
	wc /tmp/ee491f-wc-test-files/test3
	./$< /tmp/ee491f-wc-test-files/test3
	@echo ""
	wc /tmp/ee491f-wc-test-files/test4
	./$< /tmp/ee491f-wc-test-files/test4
	@echo ""
	wc /tmp/ee491f-wc-test-files/test5
	./$< /tmp/ee491f-wc-test-files/test5
