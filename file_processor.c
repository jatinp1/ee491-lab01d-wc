#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
void processor (FILE *filePointer, int mode, char* fileName) {
   char character = fgetc(filePointer);
   char space_utf = '\0';
   int character_count = 0, word_count = 0, line_count = 0;
   bool in_word = false;
   int const new_line = 10;
   //feof returns 0 if filePointer is not EOF character
   while(0 == feof(filePointer)) {
      if (mode == 0 || mode == 1)
         character_count++;
      if (!isspace(character) && !in_word && (character != space_utf) && (mode == 3 || mode == 0)) {
        in_word = true;
        word_count++;
      }
      else if (isspace(character) && in_word && (mode == 3 || mode == 0)) {
       in_word = false;
       }
      // Is this a new line?
      if (character == new_line && (mode == 2 || mode == 0))
         line_count++;
      character = fgetc(filePointer);
   }

   // Output count of line, word, and character of file.

   switch (mode) {
      case 1:
         fprintf(stdout,"%d \t [%s]\n",character_count, fileName);
         break;
      case 2:
         fprintf(stdout,"%d \t [%s]\n",line_count, fileName);
         break;
      case 3:
         fprintf(stdout,"%d \t [%s]\n",word_count, fileName);
         break;
      default:
         fprintf(stdout,"%d \t %d \t %d \t [%s]\n",line_count, word_count, character_count, fileName);
   }
}
