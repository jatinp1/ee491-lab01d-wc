///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file wc.c
/// @version 2.0
///
/// @author Jatin Pandya <jatinp@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   16_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "file_processor.h"
#include "close_file.h"

int main(int argc, char *argv[]) {
   // Check if there is the file name
   if (argc < 2){
      fprintf(stderr,"Usage:  wc FILE\n");
      exit(EXIT_FAILURE);
   }
   int mode = 0;
   if (!strcmp(argv[argc-1], "-c") || !strcmp(argv[argc-1], "--bytes")  )
      mode = 1;
   if (!strcmp(argv[argc-1], "-l") || !strcmp(argv[argc-1], "--lines")  )
      mode = 2;
   if (!strcmp(argv[argc-1], "-w") || !strcmp(argv[argc-1], "--words")  )
      mode = 3;
   if (!strcmp(argv[argc-1], "--version"))
      mode = 4;

   if (mode == 4) {
      fprintf(stdout, "%s version 2.0\n", argv[0]);
      exit (EXIT_SUCCESS);
   }
   int numFiles = argc;
   if (mode > 0) 
      numFiles -= 1;

   for (int i = 1; i < numFiles; i++) {
    FILE *filePointer = NULL;
   //First open file to see if it exists
   filePointer = fopen(argv[i], "r");
   if (filePointer == NULL){
      fprintf(stderr,"%s: Can’t open [%s]\n",argv[0], argv[i]);
      exit(EXIT_FAILURE);
   }
   // Start processing through file
   processor(filePointer, mode, argv[i]);
   // Check if file is closed
   file_close(filePointer);
   
   }
  return EXIT_SUCCESS;
}
